<?php

// -------------------------------------------------------------------------- //
// 1 - Update User Model |
// --------------------- *

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject {

  use Notifiable;

  // Rest omitted for brevity

  /**
   * Get the identifier that will be stored in the subject claim of the JWT.
   *
   * @return mixed
   */
  public function getJWTIdentifier() {

    return $this->getKey();

  }

  /**
   * Return a key value array, containing any custom claims to be added to the JWT.
   *
   * @return array
   */
  public function getJWTCustomClaims() {

    return [];

  }

}

// -------------------------------------------------------------------------- //
// 2 - Configure Auth Guard |
// ------------------------ *

/** ---------------- *
* In config/auth.php |
* ------------------ *
*
*  'defaults' => [
*     'guard' => 'api',
*     'passwords' => 'users',
* ],
*
* ...
*
* 'guards' => [
*     'api' => [
*         'driver' => 'jwt',
*         'provider' => 'users',
*     ],
* ],
*
**/

// -------------------------------------------------------------------------- //
// 3 - Add jwt-auth middleware |
// --------------------------- *

/** ------------------------------- *
* In app/Http/Middleware/kernel.php |
* --------------------------------- *
*
* protected $routeMiddleware = [
*   ...
*   'jwt.auth' => Tymon\JWTAuth\Http\Middleware\Authenticate::class,
*   'jwt.check' => Tymon\JWTAuth\Http\Middleware\Check::class,
*   'jwt.refresh' => Tymon\JWTAuth\Http\Middleware\RefreshToken::class,
* ];
*
**/

// -------------------------------------------------------------------------- //
// 4 - Update API RouteServiceProvider namespace |
// --------------------------------------------- *

/** ----------------------------------- *
* In Providers/RouteServiceProvider.php |
* ------------------------------------- *
*
* protected function mapApiRoutes() {
*
*   Route::prefix('api')
*        ->middleware('api')
*        ->namespace($this->namespace.'\API')
*        ->group(base_path('routes/api.php'));
*
* }
*
**/

// -------------------------------------------------------------------------- //
// 5 - Add basic authentication routes |
// ----------------------------------- *

/** --------------- *
* In routes/api.php |
* ----------------- *
*
* Route::group(['middleware' => 'api', 'prefix' => 'auth'], function($router) {
*
*   Route::post('login', 'AuthController@login');
*   Route::post('logout', 'AuthController@logout');
*   Route::post('refresh', 'AuthController@refresh');
*   Route::post('me', 'AuthController@me');
*
*   Route::get('online', 'AuthController@online')->middleware('jwt.refresh');
*
* });
*
**/

// -------------------------------------------------------------------------- //
// 6 - Create the AuthController |
// ----------------------------- *

/**
* Run : php artisan make:controller API/AuthController
**/

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller {

  /**
   * Create a new AuthController instance.
   *
   * @return void
   */
  public function __construct() {

    $this->middleware('auth:api', ['except' => ['login']]);

  }

  /**
   * Get a JWT via given credentials.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function login(Request $request) {

    // Parameters ----------------------------------------------------------- //

    $credentials = request(['email', 'password']);

    // Login attempt -------------------------------------------------------- //

    if(! $token = auth()->attempt($credentials)) {

      return response()->json(['error' => 'Unauthorized'], 401);

    }

    // Response ------------------------------------------------------------- //

    return $this->respondWithToken($token);

  }

  /**
   * Get the authenticated User.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function me() {

    return response()->json(auth()->user());

  }

  /**
   * Log the user out (Invalidate the token).
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout() {

    auth()->logout();

    return response()->json(['message' => 'Successfully logged out']);

  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh() {

    return $this->respondWithToken(auth()->refresh());

  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token) {

    return response()->json([
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' => auth()->factory()->getTTL() * 60
    ]);

  }

  /**
   * Simple check that the user is still authenticated.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function online() {

    return response()->json(['online' => true]);

  }

}

// -------------------------------------------------------------------------- //
// 7 - Create the AuthController |
// ----------------------------- *

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware {

  /**
   * Get the path the user should be redirected to when they are not authenticated.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return string|null
   */
  protected function redirectTo($request) {

    if(! $request->expectsJson()) {

      abort(401, 'Unauthorized');

    }

  }

}

?>
