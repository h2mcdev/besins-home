# @ - GRIMBERG PORTAIL
---

This application is the entry portal allowing access to all the applications available to the customer.

### Information about Laravel / vue installation (https://github.com/laravel/ui#installation)

`/!\ You don't need to launch those commands /!\`

1 - composer create-project laravel/laravel example-app

2 - composer require laravel/ui

3 - php artisan ui vue --auth

4 - npm install

5 - php artisan serve & npm run dev

### Laravel Dependencies

|___________________________________ Vendor ___________________________________|

* tymon/jwt-auth : https://jwt-auth.readthedocs.io/en/develop/

  * __1 - Installation:__

    * `composer require tymon/jwt-auth`

    * `php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"`

    * `php artisan jwt:secret`

  * __2 - Config: (Watch steps in storage/install/jwt-auth.php)__

    * 1 - Update User Model

    * 2 - Configure Auth Guard

    * 3 - Add jwt-auth middleware

    * 4 - Update API RouteServiceProvider namespace

    * 5 - Add basic authentication routes

    * 6 - Create the AuthController

    * 7 - Update original Authenticate Middleware

### Webapp Dependencies

|___________________________________ Plugins __________________________________|

* vue-router : https://router.vuejs.org/installation.html

  * `npm install vue-router`

* vuex : https://vuex.vuejs.org/installation.html

  * `npm install vuex --save`

* vuex-router-sync : https://www.npmjs.com/package/vuex-router-sync

  * `npm install vuex-router-sync`

* vue-async-computed : https://github.com/foxbenjaminfox/vue-async-computed

  * `npm install --save vue-async-computed`

* vue-i18n : https://kazupon.github.io/vue-i18n/installation.html#direct-download-cdn

  * `npm install vue-i18n`

* vue-moment : https://www.npmjs.com/package/vue-moment

  * `npm install vue-moment`

* raven-js : https://docs.sentry.io/clients/javascript/install/

  * `npm install raven-js --save`

* element-ui : https://github.com/ElemeFE/element

  * `npm install element-ui -S`

* twin-bcrypt : https://github.com/fpirsch/twin-bcrypt

  * `npm i twin-bcrypt`

|_________________________________ Components _________________________________|

* @fortawesome/vue-fontawesome : https://www.npmjs.com/package/@fortawesome/vue-fontawesome

  * `npm i --save @fortawesome/fontawesome-svg-core`

  * `npm i --save @fortawesome/free-solid-svg-icons`

  * `npm i --save @fortawesome/vue-fontawesome@2` (with vue-js 2.x)

|_________________________________ Directives _________________________________|

* vue-click-outside : https://www.npmjs.com/package/vue-click-outside

  * `npm install vue-click-outside`

|__________________________________ Bootstrap _________________________________|

* Popper.js.map : Put the file in **resources/bootstrap/js/**

  * (i) Check the version in **node_modules/bootstrap/package.json**

---

# @ - Global Configuration (Mac)
---

> PHP 7.2.*
> Composer 1.10.*
> Node 10.22.0
> Npm 6.14.6

---

# @ - Local Installation (Mac)
---

### Laravel Setup:

1 - Clone the git repository.

2 - Check your php version (7.2 or more) : `php -v`

3 - Check your composer version (2.0 or more) : `composer --version`

* For update version : `composer self-update`

4 - Install composer packages : `composer install`

5 - Create environment file .env : `cp .env.example .env`

6 - Generate the laravel key : `php artisan key:generate`

### FRONTEND :

1 - Check Node & npm version (10.22.x & 6.14.x) : `node -v` & `npm -v`

* For manage Node version, use **nvm** package (https://github.com/creationix/nvm)

  * `nvm install 8.16.2` or `nvm use 8.16.2`

2 - Install node modules : `npm install`

* In troubles case :

  * `rm -r node_modules`

  * `rm package-lock.json`

  * `npm install`

### RUN :

__ON SEPERATED TERMINAL'S TABS :__

1 - Run Laravel server : `php artisan serve`

2 - Run Vue JS : `npm run watch`

---

# @ - HELP ARTISAN
---

### Migrations:

* migrate database: `php artisan migrate`

* rollback migration: `php artisan migrate:rollback`

  * `(i)` You can specify the number of rollback's step with **--step=Number**

* rollback & run all migrations: `php artisan migrate:refresh`

* create table: `php artisan make:migration name_of_the_migration --create=table_name`

* update table: `php artisan make:migration name_of_the_migration table=table_name`


### Seeds:

* create seed: `php artisan make:seeder NameOfTheSeeder`

* run seed: `php artisan db:seed --class=NameOfTheSeeder`

  * `(i)` In trouble case : Run `composer dump-autoload`

### Eloquent ORM:

* create model: `php artisan make:model ModelName`

* create model with migration: `php artisan make:model ModelName -- migration`

  * `(i)` ModelName in singular preferably (for example: User)

### Controller (WEB):

* Create WEB Controller: `php artisan make:controller ModelController`

* Create API Controller: `php artisan make:controller API/ModelController --api`

### Resources:

* Create Resource: `php artisan make:resource Model`

* Create ResourceCollection: `php artisan make:resource ModelCollection`

### Middleware:

* Create Middleware: `php artisan make:middleware MiddlewareName`

  * `/!\` Don't forget to add it in **app/Http/Kernel.php** `/!\`

### Commands:

* Create command: `php artisan make:command CommandName`

### Jobs:

* Create jobs table: `php artisan queue:table` (will create jobs migration)

* Redis configuration: (Watch in **config/queue.php**)

  * set in **.env** : `QUEUE_CONNECTION=database`

* Create job: `php artisan make:job JobName`
