const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
require('laravel-mix-workbox');

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .copy('resources/images', 'public/images')
   .copy('resources/bootstrap/js', 'public/js') // Bootstrap js files
   .injectManifest({
     swSrc: './resources/js/sw.js',
     maximumFileSizeToCacheInBytes: 5000000000,
   });
