<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CibleValue extends Model
{
    protected $connection = 'crm';
    protected $table = 'cible_values';
}
