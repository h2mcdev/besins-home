<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApcOrder extends Model
{
    protected $connection = 'orders';
    protected $table = 'orders';
}
