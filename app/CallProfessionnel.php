<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallProfessionnel extends Model
{
    protected $connection = 'crm';
    protected $table = 'call_professionnel';
}
