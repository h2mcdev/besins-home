<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sectorisation extends Model {

// ========================================================================== //
// -------------------------------- Settings -------------------------------- //
// ========================================================================== //

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nom',
    'prenom',
    'matricule',
    'email',
    'password',
    'role',
    'role_2',
    // Special data --------------------------------------------------------- //
    'can_work_as',
    'work_as'
  ];

// ========================================================================== //
// -------------------------- ACCESSORS & MUTATORS -------------------------- //
// ========================================================================== //

  /**
   * Get the sectorisation's vm full name.
   *
   * @return array
   */
  public function getNomPrenomVmAttribute() {

    return $this->attributes['nom_vm'].' '.$this->attributes['prenom_vm'];

  }

  /**
   * Get the sectorisation's dr full name.
   *
   * @return array
   */
  public function getNomPrenomDrAttribute() {

    return $this->attributes['nom_dr'].' '.$this->attributes['prenom_dr'];

  }

}
