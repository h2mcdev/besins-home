<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterdataCible extends Model
{
    protected $connection = 'dashboard';
    protected $table = "masterdata_suivi_cible";
}
