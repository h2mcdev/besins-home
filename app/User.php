<?php

namespace App;

// Auth
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

// JWTAuth
use Tymon\JWTAuth\Contracts\JWTSubject;

// Notification
use Illuminate\Notifications\Notifiable;

// Facades
use Illuminate\Support\Facades\DB;

// Helpers
use App\Helpers\UtilsHelper;

class User extends Authenticatable implements JWTSubject {

// ========================================================================== //
// -------------------------------- Settings -------------------------------- //
// ========================================================================== //

  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nom',
    'prenom',
    'matricule',
    'email',
    'password',
    'app_autologin_token',
    'role',
    'role_2',
    // Special data --------------------------------------------------------- //
    'can_work_as',
    'work_as'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password',
    'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
    'is_admin' => 'boolean'
  ];

// ========================================================================== //
// --------------------------------- JWTAuth -------------------------------- //
// ========================================================================== //

  /**
   * Get the identifier that will be stored in the subject claim of the JWT.
   *
   * @return mixed
   */
  public function getJWTIdentifier() {

    return $this->getKey();

  }

  /**
   * Return a key value array, containing any custom claims to be added to the JWT.
   *
   * @return array
   */
  public function getJWTCustomClaims() {

    return [];

  }

// ========================================================================== //
// -------------------------- Accessors & Mutators -------------------------- //
// ========================================================================== //

  /**
  * Getters ----------------------------------------------------------------- //
  **/

  /**
   * Get the user's can_work_as.
   *
   * @return boolean
   */
  public function getFullnameAttribute() {

    return "$this->nom $this->prenom";

  }

  /**
   * Get the user's can_work_as.
   *
   * @return boolean
   */
  public function getCanWorkAsAttribute() {

    return boolval($this->attributes['can_work_as']);

  }

  /**
  * Setters ----------------------------------------------------------------- //
  **/

  /**
   * Set the user's work_as.
   *
   * @param  integer $user_id
   * @return void
   */
  public function setWorkAsAttribute($user_id) {

    $this->attributes['work_as'] = intval($user_id) ? $user_id : 0;

  }

  /**
  * Specifics --------------------------------------------------------------- //
  **/

  /**
   * Set the user's pays (allows to join all sectorisations to user).
   *
   * @return void
   */
  private function join_full_sectorisation() {

    $this->attributes['pays'] = 'France';

  }

  /**
   * Check if user's role 2 is enabled.
   *
   * @return boolean
   */
  private function role_2_enabled() {

    // Check user has role_2 => FALSE
    if(!$this->role_2) { return false; }

    // Check if not exists => TRUE
    if(!array_key_exists('role_2_enabled', $this->attributes)) { return true; }

    // Check attribute value
    return boolval($this->attributes['role_2_enabled']);

  }

  /**
   * Disable user's role 2.
   *
   * @return void
   */
  private function disable_role_2() {

    $this->attributes['role_2_enabled'] = false;

  }

// ========================================================================== //
// ------------------------------- Constantes ------------------------------- //
// ========================================================================== //

  /**
  * Children ---------------------------------------------------------------- //
  **/

  /**
   * Allow to define children of user by role.
   *
   * @var array
   */
  public const CHILDREN_ROLES = [
    'siege'  => ['vm'],
    'dir_oc' => ['vm'],
    'dz'     => ['vm'],
    'vm'     => ['']
  ];

// ========================================================================== //
// -------------------------------- Functions ------------------------------- //
// ========================================================================== //

  /**
  * Sectorisation ----------------------------------------------------------- //
  **/

  /**
   * Get user's region list
   *
   * @return array
   */
  public function regions() {

    return $this->sectorisations->pluck('region')->unique()->sort()->toArray();

  }

  /**
   * Get user's sector list
   *
   * @return array
   */
  public function sectors() {

    return $this->sectorisations->pluck('secteur')->unique()->sort()->toArray();

  }

  /**
   * Get user's uga list
   *
   * @return array
   */
  public function ugas() {

    return $this->sectorisations->pluck('uga')->unique()->sort()->toArray();

  }

  /**
   * Get user's libelle secteur (Only for Vms)
   *
   * @return array
   */
  public function sector_label() {

    if($this->check_role(['vm'])) {

      return collect($this->sectors())->first().' '.$this->fullname;

    }

    return '';

  }

  /**
   * Get user's sector sorting
   *
   * @return array
   */
  public function sector_sorting() {

    return collect($this->sectors())->transform(function($sector, $index) {

      return UtilsHelper::leading_length($sector);

    })->implode('_');

  }

  /**
  * Role -------------------------------------------------------------------- //
  **/

  /**
   * Check user's role (and role_2 by default)
   *
   * @param  array   $roles,
   * @return string
   */
  public function check_role($roles = []) {

    // With role 2 ---------------------------------------------------------- //

    if($this->role_2_enabled()) {

      return in_array($this->role, $roles) || in_array($this->role_2, $roles);

    }

    // Without role 2 ------------------------------------------------------- //

    return in_array($this->role, $roles);

  }

  /**
  * Children ---------------------------------------------------------------- //
  **/

  /**
   * Fetch children roles with second role or not
   *
   * @return array
   */
  private function children_roles() {

    // Init children roles
    $roles = [self::CHILDREN_ROLES[$this->role]];

    // Complete for the second role
    if($this->role_2) {

      $roles[] = self::CHILDREN_ROLES[$this->role_2];

    }

    return $roles;

  }

  /**
   * Allows to get informations of chidren users depending to his role
   *
   * @return array
   */
  public function children_users() {

    return User::whereIn('role', $this->children_roles())
               ->orderBy('role')
               ->get()
               ->reduce(function($memo, $user) {

                 // Disable role 2
                 $user->disable_role_2();

                 // Check user's sectorisation
                 if($user->sectorisations->isNotEmpty()) {

                   if(!array_diff($user->ugas(), $this->ugas())) {

                     $memo[] = [
                       'id'              => $user->id,
                       'nom'             => $user->nom,
                       'prenom'          => $user->prenom,
                       'matricule'       => $user->matricule,
                       'email'           => $user->email,
                       'role'            => $user->role,
                       'is_admin'        => $user->is_admin,
                       // Data complement
                       'region'          => $user->regions(),
                       'secteur'         => $user->sectors(),
                       'libelle_secteur' => $user->sector_label(),
                       // Sorting
                       'sorting'         => $user->sector_sorting()
                     ];

                   }

                 }

                 return $memo;

               }, []);

  }

  /**
   * Get user data by checking before if he's working as another user
   * If user is working as another one, set some data from this user
   *
   * @return User
   */
  public function work_as_checking() {

    if($this->work_as > 0) {

      // Get data from the other user
      $work_as_user = User::find($this->work_as);

      // Set some data
      $this->attributes['id']        = $work_as_user->id;
      $this->attributes['nom']       = $work_as_user->nom;
      $this->attributes['prenom']    = $work_as_user->prenom;
      $this->attributes['matricule'] = $work_as_user->matricule;
      $this->attributes['email']     = $work_as_user->email;
      $this->attributes['is_admin']  = $work_as_user->is_admin;
      $this->attributes['role']      = $work_as_user->role;

    }

    return $this;

  }

  /**
   * Reset work_as attribute to 0
   *
   * @return void
   */
  public function reset_work_as() {

    if($this->work_as > 0) {

      $this->update(['work_as' => 0]);

    }

  }

// ========================================================================== //
// ------------------------------ Relationships ----------------------------- //
// ========================================================================== //

  /**
   * Get the sectorisations of the user
   *
   * @return App\Models\Sectorisation
   */
  public function sectorisations() {

    // France level --------------------------------------------------------- //

    if($this->check_role(['siege', 'dir_oc'])) {

      $this->join_full_sectorisation();

      return $this->hasMany('App\Models\Sectorisation', 'pays', 'pays');

    }

    // Region level --------------------------------------------------------- //

    if($this->check_role(['dz'])) {

      return $this->hasMany('App\Models\Sectorisation', 'email_dr', 'email');

    }

    // Secteur level -------------------------------------------------------- //

    if($this->check_role(['vm'])) {

      return $this->hasMany('App\Models\Sectorisation', 'email_vm', 'email');

    }

    // Unknown role --------------------------------------------------------- //

    return collect([]);

  }

}
