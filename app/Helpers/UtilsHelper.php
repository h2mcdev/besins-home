<?php

namespace App\Helpers;

/**
 * Contains some utils variables and functions
 *
 * @package App\Helpers
 */
class UtilsHelper {

// ========================================================================== //
// --------------------------------- Global --------------------------------- //
// ========================================================================== //

  /**
   * Allow to add leading zero into number
   *
   * @param  integer|string $target
   * @param  integer $quantity
   * @return string
   */
  public static function leading_zero($target, $quantity = 2) {

    return sprintf('%0'.$quantity.'d', intval($target));

  }

  /**
   * Allow to add leading length into target
   *
   * @param  integer|string $target
   * @return string
   */
  public static function leading_length($target) {

    return strlen("$target").'$'.$target;

  }

// ========================================================================== //
// -------------------------------- Database -------------------------------- //
// ========================================================================== //

  /**
   * Parse value for nullable field
   *
   * @param  mixed  $value
   * @return string
   */
  public static function nullable($value) {

    return in_array($value, ['NULL', 'Null', 'null', '']) ? null : $value;

  }

  /**
   * Parse value for timestamp field
   *
   * @param  mixed  $date
   * @return string
   */
  public static function timestamp($date) {

    // Null case
    if(in_array($date, ['NULL', 'Null', 'null', ''])) {

      return null;

    }

    return date_format(date_create($date), 'Y-m-d H:i:s');

  }

}
