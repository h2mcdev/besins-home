<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilEtablissement extends Model
{
    protected $connection = 'crm';
    protected $table = 'profil_etablissements';
}
