<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallType extends Model
{
    protected $connection = "crm";
    protected $table = "call_types";
}
