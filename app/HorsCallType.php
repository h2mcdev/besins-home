<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorsCallType extends Model
{
    protected $connection = "crm";
    protected $table = "hors_call_types";
}
