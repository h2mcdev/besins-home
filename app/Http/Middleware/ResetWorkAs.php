<?php

namespace App\Http\Middleware;

use Closure;

// Facades
use Illuminate\Support\Facades\Auth;

class ResetWorkAs {

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) {

    if(boolval($request->header('ResetWorkAs'))) {

      auth()->user()->reset_work_as();

    }

    return $next($request);

  }

}
