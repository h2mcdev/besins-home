<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource {

  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request) {

// ------------------------------- Parameters ------------------------------- //
// -------------------------------------------------------------------------- //

    // Before "work_as" checking ----------------------------------------- //

    // Get children users of Auth user
    $children_users = $this->children_users();

    // Get Authenticated user fullname
    $original_fullname = $this->fullname;

    // Apply "work_as" checking ------------------------------------------ //

    $this->work_as_checking();

// ------------------------------- Data model ------------------------------- //
// -------------------------------------------------------------------------- //

    $result = [
      // "work_as" dependent data ------------------------------------------- //
      'id'                         => (int) $this->id,
      'nom'                        => (string) $this->nom,
      'prenom'                     => (string) $this->prenom,
      'matricule'                  => (string) $this->matricule,
      'email'                      => (string) $this->email,
      'is_admin'                   => (boolean) $this->is_admin,
      'role'                       => (string) $this->role,
      'role_2'                     => (string) $this->role_2,
      'can_work_as'                => (boolean) $this->can_work_as,
      'work_as'                    => (integer) $this->work_as,
      'fullname'                   => (string) $this->fullname,
      //'app_autologin_token'        => (string) $this->app_autologin_token,
      // "work_as" undependent data ----------------------------------------- //
      'children_users'             => $children_users,
      'original_fullname'          => $original_fullname
    ];

// -------------------------------------------------------------------------- //

    return $result;

  }

}
