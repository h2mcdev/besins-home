<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SectorisationCollection extends ResourceCollection {

  /**
   * Transform the resource collection into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request) {

// ------------------------------- Data model ------------------------------- //
// -------------------------------------------------------------------------- //

    return $this->collection->transform(function($sectorisation, $index) {

      return [
        'reseau'     => $sectorisation->reseau,
        'pays'       => $sectorisation->pays,
        'region'     => $sectorisation->region,
        'nom_region' => $sectorisation->nom_region,
        'secteur'    => $sectorisation->secteur,
        'uga'        => $sectorisation->uga,
        'nom_vm'     => $sectorisation->nom_vm,
        'prenom_vm'  => $sectorisation->prenom_vm,
        'email_vm'   => $sectorisation->email_vm,
        'nom_dr'     => $sectorisation->nom_dr,
        'prenom_dr'  => $sectorisation->prenom_dr,
        'email_dr'   => $sectorisation->email_dr,
        'statut'     => $sectorisation->statut,
      ];

    });

  }

}
