<?php

namespace App\Http\Controllers\API;

use App\ApcOrder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Call;
use App\CallProfessionnel;
use App\CibleValue;
use App\Etablissement;
use App\MasterdataCible;
use App\MasterdataVentesGlobale;
use App\Models\Sectorisation;
use App\Professionnel;
use App\ProfilEtablissement;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AlertController extends Controller
{
    public function getPharmaWithoutFac(Request $request)
    {
        $usr = Auth::user();
        $request->isvm = $request->isvm == "true" ? true : false;
        if(!Auth::check()) return response("Erreur authentification", 403);
        $secteur = null;
        if($usr->role == "vm" && $usr->role_2 != "dz") {
            $secteur = Sectorisation::select("secteur")->distinct()->where("email_vm", "=", $usr->email)->get()->map(function($obj){
                return $obj->secteur;
            })->all();
        } elseif($usr->role_2 == "dz") {
            if($request->isvm){
                $secteur = Sectorisation::select("secteur")->distinct()->where("email_vm", "=", $usr->email)->get()->map(function($obj){
                    return $obj->secteur;
                })->all();
            } else {
                $secteur = Sectorisation::select("secteur")->distinct()->where("nom_dr", "=", $usr->nom)
                ->where("prenom_dr", "=", $usr->prenom)
                ->get()->map(function($obj){
                    return $obj->secteur;
                })->all();
            }
        }
        $date_delivery = date("Y-m-d", strtotime("now - 3 months"));
        $order = new ApcOrder;
        $vente_globale = new MasterdataVentesGlobale;
        $etab = new Etablissement;
        $profil_etab = new ProfilEtablissement;
        $crm_connection = $etab->getConnection()->getDatabaseName();

        $datas = MasterdataCible::join("$crm_connection.{$etab->getTable()} as etab", "eurodep_code", "=", "etab.identifiant_structure")
        ->join("$crm_connection.{$profil_etab->getTable()} as profil", "profil.etablissement_id", "=", "etab.id")
        ->select("etab.id", "pharma_cip as cip", "etab.nom as raison_sociale", "prenom as professionnel", "profil.adresse", "cp as code_postal", "profil.ville", "profil.telephone")
        ->distinct()
        ->where("spe", "=", "PO")
        ->where("profil.eurodep_adr_index", "=", 1)
        ->whereNull("etab.deleted_at")
        ->when($secteur, function($query) use($secteur){
            $query->whereIn("secteur", $secteur);
        })
        ->whereRaw("not exists (select a.id from `{$order->getConnection()->getDatabaseName()}`.`{$order->getTable()}` a 
		where a.pharma_cip = masterdata_suivi_cible.pharma_cip and a.status = 'ENVOYEE_EURODEP' and a.deliverydate >= '$date_delivery')")
        ->whereRaw("not exists (select * from `{$vente_globale->getConnection()->getDatabaseName()}`.`{$vente_globale->getTable()}` a
		where a.gers = masterdata_suivi_cible.pharma_cip and a.datfac >= '$date_delivery')");

        return response()->json([
            "count" => $datas->count(),
            "rows" => $datas->get()->all()
        ]);
    }

    public function getPharmaWithoutVisit(Request $request)
    {
        $usr = Auth::user();
        $request->isvm = $request->isvm == "true" ? true : false;
        if(!Auth::check()) return response("Erreur authentification", 403);
        $secteur = null;
        if($usr->role == "vm" && $usr->role_2 != "dz") {
            $secteur = Sectorisation::select("secteur")->distinct()->where("email_vm", "=", $usr->email)->get()->map(function($obj){
                return $obj->secteur;
            })->all();
        } elseif($usr->role_2 == "dz") {
            if($request->isvm){
                $secteur = Sectorisation::select("secteur")->distinct()->where("email_vm", "=", $usr->email)->get()->map(function($obj){
                    return $obj->secteur;
                })->all();
            } else {
                $secteur = Sectorisation::select("secteur")->distinct()->where("nom_dr", "=", $usr->nom)
                ->where("prenom_dr", "=", $usr->prenom)
                ->get()->map(function($obj){
                    return $obj->secteur;
                })->all();
            }
        }
        $date_delivery = date("Y-m-d", strtotime("now - 3 months"));
        $call = new Call;
        $etab = new Etablissement;
        $profil_etab = new ProfilEtablissement;
        $crm_connection = $etab->getConnection()->getDatabaseName();

        $datas = MasterdataCible::join("$crm_connection.{$etab->getTable()} as etab", "eurodep_code", "=", "etab.identifiant_structure")
        ->join("$crm_connection.{$profil_etab->getTable()} as profil", "profil.etablissement_id", "=", "etab.id")
        ->select("etab.id", "pharma_cip as cip", "etab.nom as raison_sociale", "prenom as professionnel", "profil.adresse", "cp as code_postal", "profil.ville", "profil.telephone")
        ->distinct()
        ->where("spe", "=", "PO")
        ->where("profil.eurodep_adr_index", "=", 1)
        ->when($secteur, function($query) use($secteur){
            $query->whereIn("secteur", $secteur);
        })
        ->whereNull("etab.deleted_at")
        ->whereRaw("not exists (select a.id from `{$call->getConnection()->getDatabaseName()}`.`{$call->getTable()}` a 
		where a.call_type_id = 4 and a.etablissement_id = masterdata_suivi_cible.crm_etab_id and a.date_debut >= '$date_delivery')");

        return response()->json([
            "count" => $datas->count(),
            "rows" => $datas->get()->all()
        ]);
    }

    public function getMedsNotView(Request $request)
    {
        $usr = Auth::user();
        $request->isvm = $request->isvm == "true" ? true : false;
        if(!Auth::check()) return response("Erreur authentification", 403);
        $secteur = null;
        if($usr->role == "vm" && $usr->role_2 != "dz") {
            $secteur = Sectorisation::select("secteur")->distinct()->where("email_vm", "=", $usr->email)->get()->map(function($obj){
                return $obj->secteur;
            })->all();
        } elseif($usr->role_2 == "dz") {
            if($request->isvm){
                $secteur = Sectorisation::select("secteur")->distinct()->where("email_vm", "=", $usr->email)->get()->map(function($obj){
                    return $obj->secteur;
                })->all();
            } else {
                $secteur = Sectorisation::select("secteur")->distinct()->where("nom_dr", "=", $usr->nom)
                ->where("prenom_dr", "=", $usr->prenom)
                ->get()->map(function($obj){
                    return $obj->secteur;
                })->all();
            }
        }
        $date_delivery = date("Y-m-d", strtotime("now - 4 months"));
        $call = new Call;
        $call_professionnel = new CallProfessionnel;
        $etab = new Etablissement;
        $profil_etab = new ProfilEtablissement;
        $crm_connection = $etab->getConnection()->getDatabaseName();
        $professionnel = new Professionnel;

        $datas = MasterdataCible::from("masterdata_suivi_cible as cible")
        ->join("$crm_connection.{$etab->getTable()} as etab", "cible.etablissement_id", "=", "etab.id")
        ->join("$crm_connection.{$profil_etab->getTable()} as profil", "profil.etablissement_id", "=", "etab.id")
        ->select("etab.id", "professionnel_id", "etab.nom as raison_sociale", DB::raw("CONCAT(cible.nom, ' ', cible.prenom) as professionnel"), "profil.adresse", "cible.cp as code_postal", "profil.ville", "profil.telephone")
        ->distinct()
        ->whereNull("etab.deleted_at")
        ->when($secteur, function($query) use($secteur){
            $query->whereIn("secteur", $secteur);
        })
        ->whereRaw("not exists (select a.id from `{$call->getConnection()->getDatabaseName()}`.`{$call->getTable()}` a 
        join `{$call_professionnel->getConnection()->getDatabaseName()}`.`{$call_professionnel->getTable()}` b 
        on a.id = b.call_id 
		where b.professionnel_id = cible.professionnel_id and a.hors_call = 0 
		and a.date_debut >= '$date_delivery' and a.deleted_at is null)");

        return response()->json([
            "count" => $datas->count(),
            "rows" => $datas->get()->all()
        ]);
    }

    public function getMedsWithoutNextVisit(Request $request)
    {
        $usr = Auth::user();
        $request->isvm = $request->isvm == "true" ? true : false;
        if(!Auth::check()) return response("Erreur authentification", 403);
        $secteur = null;
        if($usr->role == "vm" && $usr->role_2 != "dz") {
            $secteur = Sectorisation::select("secteur")->distinct()->where("email_vm", "=", $usr->email)->get()->map(function($obj){
                return $obj->secteur;
            })->all();
        } elseif($usr->role_2 == "dz") {
            if($request->isvm){
                $secteur = Sectorisation::select("secteur")->distinct()->where("email_vm", "=", $usr->email)->get()->map(function($obj){
                    return $obj->secteur;
                })->all();
            } else {
                $secteur = Sectorisation::select("secteur")->distinct()->where("nom_dr", "=", $usr->nom)
                ->where("prenom_dr", "=", $usr->prenom)
                ->get()->map(function($obj){
                    return $obj->secteur;
                })->all();
            }
        }
        $date_delivery = date("Y-m-d", strtotime("now - 4 months"));
        $call = new Call;
        $call_professionnel = new CallProfessionnel;
        $cible_value = new CibleValue;
        $etab = new Etablissement;
        $profil_etab = new ProfilEtablissement;
        $crm_connection = $etab->getConnection()->getDatabaseName();

        $datas = MasterdataCible::from("masterdata_suivi_cible as cible")
        ->join("$crm_connection.{$etab->getTable()} as etab", "cible.etablissement_id", "=", "etab.id")
        ->join("$crm_connection.{$profil_etab->getTable()} as profil", "profil.etablissement_id", "=", "etab.id")
        ->select("etab.id", "professionnel_id", "etab.nom as raison_sociale", DB::raw("CONCAT(cible.nom, ' ', cible.prenom) as professionnel"), "profil.adresse", "cible.cp as code_postal", "profil.ville", "profil.telephone")
        ->distinct()
        ->whereNull("etab.deleted_at")
        ->when($secteur, function($query) use($secteur){
            $query->whereIn("secteur", $secteur);
        })
        ->whereRaw("not exists (select a.id from `{$call->getConnection()->getDatabaseName()}`.`{$call->getTable()}` a 
        join `{$call_professionnel->getConnection()->getDatabaseName()}`.`{$call_professionnel->getTable()}` b 
        on a.id = b.call_id 
		where b.professionnel_id = cible.professionnel_id and a.hors_call = 0 
		and a.date_debut >= '$date_delivery'  and a.deleted_at is null)")
        ->whereRaw("exists (select * from `{$cible_value->getConnection()->getDatabaseName()}`.`{$cible_value->getTable()}` a 
        where a.model_id = cible.professionnel_id and a.value = 'Reçoit sur RDV' 
        and a.cible_id = 1  and a.deleted_at is null)");

        return response()->json([
            "count" => $datas->count(),
            "rows" => $datas->get()->all()
        ]);
    }

}
