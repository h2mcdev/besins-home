<?php

namespace App\Http\Controllers\API;

use App\Call;
use App\CallProfessionnel;
use App\CallType;
use App\Etablissement;
use App\Expense;
use App\ExpenseSheet;
use App\HorsCallType;
use App\Http\Controllers\Controller;
use App\Professionnel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ActivityController extends Controller
{
    public function getYearMonth()
    {
        $date = date("Y-m-d", strtotime("now -1 year"));
        return Call::selectRaw("DATE_FORMAT( date_debut, '%Y-%m') as `year_month`")->distinct()->whereRaw("date_debut >= '$date' AND date_debut <= NOW()")->orderBy("year_month", "desc")->get()->map(function($obj){
            return $obj->year_month;
        })->all();
    }

    public function getDatas(Request $request)
    {
        $data = $request->all();
        $year_month = $data["yearmonth"];
        $year = intval(substr($year_month, 0,4));
        $month = intval(substr($year_month, -2));
        $days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $user_selected = $data["user"];
        $user = User::find($user_selected) ?? Auth::user();
        $daysList= ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"];

        $datas = [];

        $holidays = $this->getHolidays($year);

        if($user->role != "siege"){
            for ($i=0; $i < $days_in_month; $i++) { 
                $day = $i+1 < 10 ? "0".$i+1 : $i+1;
                $day_in_week = date("w", strtotime("$year_month-$day"));
                $current_date = date("Y-m-d", strtotime("$year_month-$day"));
    
                $call_morning = Call::select("call_type_id")
                ->where("creator_id", "=", $user->id)
                ->whereNotNull("call_type_id")
                ->where("date_debut", "LIKE", "$current_date%")
                ->whereRaw("CONVERT_TZ(date_debut, 'UTC', 'Europe/Paris') <= '$current_date 13:00:00'")
                ->where("statut", "=", "REALISE")
                ->orderBy("call_type_id")
                ->get()->first();
    
                $call_type_morning = !empty($call_morning) ? CallType::find($call_morning->call_type_id) : null;

                $hors_call_morning = Call::select("hors_call_type_id")
                ->distinct()
                ->where("creator_id", "=", $user->id)
                ->where("date_debut", "LIKE", "$current_date%")
                ->whereRaw("CONVERT_TZ(date_debut, 'UTC', 'Europe/Paris') <= '$current_date 13:00:00'")
                ->where("statut", "=", "REALISE")
                ->where("hors_call", 1)
                ->orderBy("hors_call_type_id")
                ->get()->map(function($item){
                    return $item->hors_call_type_id;
                })->all();

                $hors_call_type_morning = "";
                foreach ($hors_call_morning as $key => $hors_call) {
                    $hc = HorsCallType::find($hors_call);
                    if($key == 0) $hors_call_type_morning .= "$hc->value ";
                    else $hors_call_type_morning .= "/ $hc->value ";
                }
    
                $call_afternoon = Call::select("call_type_id")
                ->where("creator_id", "=", $user->id)
                ->whereNotNull("call_type_id")
                ->where("date_debut", "LIKE", "$current_date%")
                ->whereRaw("CONVERT_TZ(date_fin, 'UTC', 'Europe/Paris') >= '$current_date 13:00:00'")
                ->where("statut", "=", "REALISE")
                ->orderBy("call_type_id")
                ->get()->first();
    
                $call_type_afternoon = !empty($call_afternoon) ? CallType::find($call_afternoon->call_type_id) : null;

                $hors_call_afternoon = Call::select("hors_call_type_id")
                ->distinct()
                ->where("creator_id", "=", $user->id)
                ->where("date_debut", "LIKE", "$current_date%")
                ->whereRaw("CONVERT_TZ(date_fin, 'UTC', 'Europe/Paris') >= '$current_date 13:00:00'")
                ->where("statut", "=", "REALISE")
                ->where("hors_call", 1)
                ->orderBy("hors_call_type_id")
                ->get()->map(function($item){
                    return $item->hors_call_type_id;
                })->all();

                $hors_call_type_afternoon = "";
                foreach ($hors_call_afternoon as $key => $hors_call) {
                    $hc = HorsCallType::find($hors_call);
                    if($key == 0) $hors_call_type_afternoon .= "$hc->value ";
                    else $hors_call_type_afternoon .= "/ $hc->value ";
                }

                $morning = $call_type_morning->value ?? $hors_call_type_morning ?? "";
                $afternoon = $call_type_afternoon->value ?? $hors_call_type_afternoon ?? "";
                if(strpos($morning, "Visite") !== false) $morning = "VIS";
                if(strpos($afternoon, "Visite") !== false) $afternoon = "VIS";
    
                $nbmed = CallProfessionnel::join((new Call)->getTable(), "call_id", "=", "calls.id")
                ->join((new Professionnel)->getTable(), "professionnel_id", "=", "professionnels.id")
                ->where("date_debut", "LIKE", "$current_date%")
                ->where("statut", "=", "REALISE")
                ->where("creator_id", "=", $user->id)
                ->whereNull("calls.deleted_at")
                ->count();
    
                $nbphar = Call::join((new Etablissement())->getTable(), "etablissement_id", "=", "etablissements.id")
                ->where("date_debut", "LIKE", "$current_date%")
                ->where("statut", "=", "REALISE")
                ->where("creator_id", "=", $user->id)
                ->where("type", "=", "PHARMACIE")
                ->whereNull("calls.deleted_at")
                ->count();
    
                $datas[$i]["date"] = $daysList[$day_in_week]. " ".$day; 
                $datas[$i]["morning"] =  $morning;
                $datas[$i]["afternoon"] = $afternoon;
                $datas[$i]["nbmed"] = $nbmed !== 0 ? $nbmed : "" ; 
                $datas[$i]["nbphar"] = $nbphar !== 0 ? $nbphar: ""; 
    
                if($daysList[$day_in_week] == "Sam" || $daysList[$day_in_week] == "Dim" || in_array($current_date, $holidays)){ 
                    $datas[$i]["_rowVariant"] = "secondary";
                }
            }
        }

        return $datas;
    }

    public function getHolidays($year)
    {
        $easterDate = easter_date($year);
        $easterDay = date('j', $easterDate);
        $easterMonth = date('n', $easterDate);
        $easterYear = date('Y', $easterDate);

        $holidays = array(
            // Jours feries fixes
            date("Y-m-d", mktime(0, 0, 0, 1, 1, $year)), // 1er janvier
            date("Y-m-d",mktime(0, 0, 0, 5, 1, $year)), // Fete du travail
            date("Y-m-d",mktime(0, 0, 0, 5, 8, $year)), // Victoire des allies
            date("Y-m-d",mktime(0, 0, 0, 7, 14, $year)),// Fete nationale
            date("Y-m-d",mktime(0, 0, 0, 8, 15, $year)),// Assomption
            date("Y-m-d",mktime(0, 0, 0, 11, 1, $year)),// Toussaint
            date("Y-m-d",mktime(0, 0, 0, 11, 11, $year)),// Armistice
            date("Y-m-d",mktime(0, 0, 0, 12, 25, $year)),// Noel

            // Jour feries qui dependent de paques
            date("Y-m-d",mktime(0, 0, 0, $easterMonth, $easterDay + 1, $easterYear)),// Lundi de paques
            date("Y-m-d",mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear)),// Ascension
            date("Y-m-d",mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear)), // Pentecote
        );

        return $holidays;
    }
}
