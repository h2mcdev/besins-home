<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Resources
use App\Http\Resources\UserResource;

class AuthController extends Controller {

// ========================================================================== //
// -------------------------------- Settings -------------------------------- //
// ========================================================================== //

  /**
   * Create a new AuthController instance.
   *
   * @return void
   */
  public function __construct() {

    $this->middleware('auth:api', ['except' => ['login']]);

  }

// ========================================================================== //
// -------------------------------- Functions ------------------------------- //
// ========================================================================== //

  /**
   * Get a JWT via given credentials.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function login(Request $request) {

    // Parameters ----------------------------------------------------------- //

    $credentials = request(['email', 'password']);

    // Login attempt -------------------------------------------------------- //

    if(! $token = auth()->attempt($credentials)) {

      return response()->json(['error' => 'Unauthorized'], 401);

    }

    $user = auth()->user();
    //$user->app_autologin_token = auth()->tokenById($user->id);
    $user->save();

    // Response ------------------------------------------------------------- //

    return $this->respondWithToken($token);

  }

  /**
   * Get the authenticated User.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function me() {

    return response()->json(new UserResource(auth()->user()));

  }

  /**
   * Log the user out (Invalidate the token).
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout() {

    auth()->logout();

    return response()->json(['message' => 'Successfully logged out']);

  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh() {

    return $this->respondWithToken(auth()->refresh());

  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token) {

    return response()->json([
      'token'      => $token,
      'user'       => new UserResource(auth()->user()),
      'expires_in' => auth()->factory()->getTTL() * 60
    ]);

  }

  /**
   * Simple check that the user is still authenticated.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function online() {

    return response()->json(['online' => true]);

  }

  /**
   * Update the user.
   *
   * @param  \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(Request $request) {

    auth()->user()->update($request->all());

    return response()->json(['success' => true]);

  }

}
