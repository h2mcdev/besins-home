<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseSheet extends Model
{
    protected $connection = "crm";
    protected $table = "expense_sheets";
}
