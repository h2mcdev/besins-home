<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Laravel initial routes --------------------------------------------------- //

// Route::middleware('auth:api')->get('/user', function(Request $request) {
//
//   return $request->user();
//
// });

// JWT auth routes ---------------------------------------------------------- //

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function($router) {

  Route::post('login', 'AuthController@login');
  Route::post('logout', 'AuthController@logout');
  Route::post('me', 'AuthController@me');
  Route::post('refresh', 'AuthController@refresh')->middleware('workAs.reset');

  Route::get('online', 'AuthController@online')->middleware('jwt.refresh');

  Route::patch('update', 'AuthController@update');

});

Route::get('get_year_month', 'ActivityController@getYearMonth');
Route::get('get_datas', 'ActivityController@getDatas');
// Route::get('get_pharma_without_fac', 'AlertController@getPharmaWithoutFac');
// Route::get('get_pharma_without_visit', 'AlertController@getPharmaWithoutVisit');
Route::get('get_meds_not_view', 'AlertController@getMedsNotView');
Route::get('get_meds_without_next_visit', 'AlertController@getMedsWithoutNextVisit');
