<?php

use Illuminate\Database\Seeder;

class FillFromSourceUsersTable extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

// -------------------------------- Security -------------------------------- //
// ========================================================================== //

    try
    {
      $users = DB::connection('source')
                 ->table('users_source')
                 ->get();
    }
    catch(\Exception $e) {

      $this->command->info("* - - - - - - - - - - - - - - - - - - - - - - |");
      $this->command->info("*_/!\_ La connection à la table à échouée _/!\_");
      $this->command->info("* - - - - - - - - - - - - - - - - - - - - - - |");
      $this->command->info("* - Database: ".env('DB_DATABASE_SOURCE'));
      $this->command->info("* - Table: users_source");

      return;

    }

// ---------------------------------- Clean --------------------------------- //
// ========================================================================== //

    DB::table('users')->delete();

    DB::statement('ALTER TABLE users AUTO_INCREMENT = 1');

// ---------------------------------- Fill ---------------------------------- //
// ========================================================================== //

    // Informations --------------------------------------------------------- //

    $this->command->info("* - - - - - - - - - - - - - - - - - - - - - - - - |");
    $this->command->info("* - Table: users");
    $this->command->info("* - Date: ".date('Y/m/d'));
    $this->command->info("* - Start at: ".date('h:i:s'));

    // Insert --------------------------------------------------------------- //

    DB::transaction(function() use ($users) {

      foreach($users as $index => $user) {

        DB::table('users')->insert([
          'id'          => $user->id,
          'nom'         => $user->nom,
          'prenom'      => $user->prenom,
          'matricule'   => $user->matricule,
          'email'       => $user->email,
          'password'    => $user->password,
          'is_admin'    => $user->is_admin,
          'role'        => $user->role,
          'role_2'      => $user->role_2,
          'can_work_as' => $user->can_work_as,
          'work_as'     => $user->work_as
        ]);

      }

    });

    $this->command->info('* - End at: '.date('h:i:s'));
    $this->command->line('');

// -------------------------------------------------------------------------- //

  }

}
