<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run() {

// ========================================================================== //
// -------------------------------- Settings -------------------------------- //
// ========================================================================== //

    // Set memory_limit & max_execution_time (uncomment if necessary)
    // ini_set('memory_limit', '-1');
    // ini_set('max_execution_time', '600'); // => 10 min

// ========================================================================== //
// --------------------------------- Seeders -------------------------------- //
// ========================================================================== //

    // * ----------
    // Installation --------------------------------------------------------- //
    // * ----------

    $this->command->line('* - - - - - - - - - - - - - - - - - - - - - - - - *');

    // Define mode installation
    $mode = $this->command->choice('Which installation mode do you want to launch ?', [
      'COMPLETE',
      'PARTIAL',
      'CANCEL'
    ], 2);

    $this->command->line('* - - - - - - - - - - - - - - - - - - - - - - - - *');

    switch($mode) {

      // Complete installation ---------------------------------------------- //
      case 'COMPLETE':

        $this->command->line('* _/!\___________________________________/!\_ *');
        $this->command->line('* Les données actuelles des tables ci-dessous');
        $this->command->line('* seront définitivement perdues.');
        $this->command->line('* - Users');
        $this->command->line('* ___________________________________________ *');

        if($this->command->confirm('Confirmer ?')) {

          $this->call(FillFromSourceUsersTable::class);

        }

        break;

      case 'PARTIAL':

        $this->command->line('* _/!\___________________________________/!\_ *');
        $this->command->line('* Les données actuelles de la table ci-dessous');
        $this->command->line('* seront définitivement perdues.');
        $this->command->line('* - Users');
        $this->command->line('* ___________________________________________ *');

        if($this->command->confirm('Confirmer ?')) {

          $this->call(FillFromSourceUsersTable::class);

        }

        break;

      case 'CANCEL':
        $this->command->line('Installation Cancelled...');
        break;

      default:
        $this->command->line('Installation Cancelled...');
        break;

    }

    $this->command->line('');
    $this->command->line('* - - - - - - - - - - - - - - - - - - - - - - - - *');
    $this->command->line('');

  }

}
