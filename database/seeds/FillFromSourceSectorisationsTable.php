<?php

use Illuminate\Database\Seeder;

class FillFromSourceSectorisationsTable extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

// -------------------------------- Security -------------------------------- //
// ========================================================================== //

    try
    {
      $count = DB::connection('source')
                 ->table('sectorisation')
                 ->count();
    }
    catch(\Exception $e) {

      $this->command->info("* - - - - - - - - - - - - - - - - - - - - - - |");
      $this->command->info("*_/!\_ La connection à la table à échouée _/!\_");
      $this->command->info("* - - - - - - - - - - - - - - - - - - - - - - |");
      $this->command->info("* - Database: ".env('DB_DATABASE_SOURCE'));
      $this->command->info("* - Table: sectorisations");

      return;

    }

// ---------------------------------- Clean --------------------------------- //
// ========================================================================== //

    DB::table('sectorisations')->delete();

    DB::statement('ALTER TABLE sectorisations AUTO_INCREMENT = 1');

// ---------------------------------- Fill ---------------------------------- //
// ========================================================================== //

    // Informations --------------------------------------------------------- //

    $this->command->info("* - - - - - - - - - - - - - - - - - - - - - - - - |");
    $this->command->info("* - Table: sectorisations");
    $this->command->info("* - Date: ".date('Y/m/d'));
    $this->command->info("* - Count of row(s) to add: $count.");
    $this->command->info("* - Start at: ".date('h:i:s'));

    // Insert --------------------------------------------------------------- //

    DB::connection('source')
      ->table('sectorisation')
      ->get()
      ->map(function($row, $index) {

        return [
          'reseau'     => $row->reseau,
          'pays'       => $row->pays,
          'region'     => $row->region,
          'nom_region' => $row->nomregion,
          'secteur'    => $row->secteur,
          'uga'        => $row->uga,
          'nom_vm'     => $row->nomvm,
          'prenom_vm'  => $row->prenomvm,
          'email_vm'   => $row->emailvm,
          'nom_dr'     => $row->nomdr,
          'prenom_dr'  => $row->prenomdr,
          'email_dr'   => $row->emaildr,
          'statut'     => $row->statutsecteur,
        ];


      })->split(ceil($count / 500))->each(function($rows) {

        DB::table('sectorisations')
          ->insert(collect($rows)->toArray());

      });

    $this->command->info('* - End at: '.date('h:i:s'));
    $this->command->line('');

// -------------------------------------------------------------------------- //

  }

}
