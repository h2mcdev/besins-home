<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAppAutologinTokenToUsersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {

    Schema::table('users', function (Blueprint $table) {

      // Columns ------------------------------------------------------------ //

      $table->text('app_autologin_token')
            ->after('remember_token')
            ->nullable();

    });

  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {

    Schema::table('users', function (Blueprint $table) {

      // Columns ------------------------------------------------------------ //

      $table->dropColumn('app_autologin_token');

    });

  }

}
