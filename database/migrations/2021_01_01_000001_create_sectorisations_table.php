<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorisationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if (!Schema::hasTable('sectorisations')) {
        Schema::create('sectorisations', function(Blueprint $table) {

          // Columns
          $table->string('reseau', 50)->default('GRIMBERG');
          $table->string('pays', 50)->default('France');
          $table->string('region', 10)->nullable();
          $table->string('nom_region', 20)->nullable();
          $table->string('secteur', 10)->nullable();
          $table->string('uga', 10)->nullable();
          $table->string('nom_vm', 100)->nullable();
          $table->string('prenom_vm', 100)->nullable();
          $table->string('email_vm', 100)->default('');
          $table->string('nom_dr', 100)->nullable();
          $table->string('prenom_dr', 100)->nullable();
          $table->string('email_dr', 100)->default('');
          $table->string('statut', 50)->nullable();;
          $table->string('siege_fk', 20)->default('siege');
  
          // Primaries keys
          $table->primary(['reseau', 'uga']);
  
          // Indexes
          $table->index('region', 'sectorisations_region_index');
          $table->index('secteur', 'sectorisations_secteur_index');
          $table->index('uga', 'sectorisations_uga_index');
          $table->index('email_vm' ,'sectorisations_email_vm_index');
          $table->index('email_dr', 'sectorisations_email_dr_index');
          $table->index(['region', 'email_dr'], 'dr');
          $table->index(['secteur', 'email_vm'], 'vm');
          $table->index('siege_fk', 'siege_fk');
  
        });

      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

      Schema::dropIfExists('sectorisations');

    }

}
