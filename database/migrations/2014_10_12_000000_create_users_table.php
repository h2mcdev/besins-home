<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {

    Schema::create('users', function(Blueprint $table) {

      // Columns ------------------------------------------------------------ //

      $table->id(); // Alias of $table->bigIncrements('id').
      $table->string('nom');
      $table->string('prenom');
      $table->string('matricule')->unique();
      $table->string('email')->unique();
      $table->string('password');
      $table->boolean('is_admin')->default(false);

      // System Columns ----------------------------------------------------- //

      $table->timestamp('email_verified_at')->nullable();
      $table->rememberToken();
      $table->timestamps();

      // Additional Columns ------------------------------------------------- //
      $table->string('type')->default('');
      $table->string('role')->default('');
      $table->string('role_2')->nullable();
      $table->boolean('can_work_as')->default(false);
      $table->unsignedInteger('work_as')->default(0);

    });

  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {

    Schema::dropIfExists('users');

  }

}
