<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<!-- ///////////////////////////////// Head //////////////////////////////// -->

  <head>

    <!-- Metas _____________________________________________________________ -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token ________________________________________________________ -->

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts ___________________________________________________________ -->

    <script src="js/app.js" defer></script>
    <!-- <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script> -->

    <!-- environment variable -->
    <script>

      window.SENTRY_PUBLIC_DSN = "{{ env('SENTRY_PUBLIC_DSN') }}";
      window.APP_VERSION       = "{{ env('APP_VERSION') }}";

    </script>

    <link rel="manifest" href="/manifest.webmanifest">

    <!-- Links _____________________________________________________________ -->

    <link rel="icon" href="/images/favicon/ms-icon-144x144.png">

    <!-- Styles ____________________________________________________________ -->

    <link href="css/app.css" rel="stylesheet">

  </head>

<!-- //////////////////////////////// Webapp /////////////////////////////// -->

  <body>

    <div id="app"></div>

  </body>

</html>
