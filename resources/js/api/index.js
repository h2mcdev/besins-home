// --------------------------------- Imports -------------------------------- //
// ========================================================================== //

// Const -------------------------------------------------------------------- //

import * as config from '../config';

// ---------------------------------- Class --------------------------------- //
// ========================================================================== //

/**
 * Api list:
 * -* Work as
 *  |- set_work_as
 */
export class Api {

  // ------------------------------------------------------------------------ //
  // Settings |
  // -------- *

  /**
   * Init attributes
   */
  constructor() {

    this.user_email = JSON.parse(localStorage.getItem(config.APP_PREFIX + 'user_data'))?.user.email;
    this.token      =  localStorage.getItem(config.APP_PREFIX + 'token');
    this.autologin_token = JSON.parse(localStorage.getItem(config.APP_PREFIX + 'user_data'))?.user.app_autologin_token;
    // For later...
    // this.use_local_db = FORCE_USE_LOCAL_DB;
    // this.offline_enabled = true;
    // this.mutations_counter = 0;
    // this.logout = () => {};

  }

  // ------------------------------------------------------------------------ //
  // Accessors / Mutators |
  // -------------------- *

  /**
   * Update api's user_email
   *
   * @param  String email
   * @return void
   */
  set_user_email(email) {

    this.user_email = email;

  }

  /**
   * Update api's token
   *
   * @param  String token
   * @return void
   */
  set_token(token) {

    this.token = token;

  }

  /**
   * Update api's autologin_token
   *
   * @param  String autologin_token
   * @return void
   */
  set_autologin_token(autologin_token) {
    this.autologin_token = autologin_token;
  }

  /**
   * Update api's logout
   *
   * @param  Function logout
   * @return void
   */
  setLogout(logout) {

    this.logout = logout;

  }

  // ------------------------------------------------------------------------ //
  // Work as |
  // ------- *

  /**
   * Update user's "work_as" attributes.
   *
   * @param  Object { work_as: Number }
   * @return Promise()
   */
  set_work_as(work_as) {

    return fetch(config.API_URL + config.API_PATH + '/auth/update', {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      },
      body: JSON.stringify(work_as)
    }).then((response) => {

      if(response.status !== 200) { // On failure --------------------------- //

        throw new Error('Une erreur est survenue. Veuillez réessayer.');

      }

      // On Success --------------------------------------------------------- //

      return Promise.resolve([])

    });

  }

}

export default (new Api());
