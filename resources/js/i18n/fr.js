export default {

  menu: {
    example: 'Exemple',
    small_screen: {
      example: 'Ex.'
    }
  },
  general: {
    cancel: 'Annuler',
    edit: 'Modifier',
    delete: 'Supprimer',
    save: 'Enregistrer',
  },

};
