// --------------------------------- Imports -------------------------------- //
// ========================================================================== //

// Dependencies ------------------------------------------------------------- //

// ------- *
// Plugins |
// ------- *

import Vue from 'vue';

import { sync } from 'vuex-router-sync';

import AsyncComputed from 'vue-async-computed';

import VueI18n from 'vue-i18n';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/fr';

import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';

import { BootstrapVue } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// --------- *
// Component |
// --------- *

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faTimes,
  faAngleUp,
  faAngleDown,
  faEye,
  faEyeSlash,
  faCheck,
  faInfo,
  faInfoCircle,
  faPowerOff
} from '@fortawesome/free-solid-svg-icons';

// ---------- *
// Directives |
// ---------- *

import ClickOutside from 'vue-click-outside';

// App ---------------------------------------------------------------------- //

import store from './store';

import router from './router';

import messages from './i18n';

import App from './components/App.vue';

// ------------------------------ Installation ------------------------------ //
// ========================================================================== //

// ------- *
// Library |
// ------- *

require('./library/bootstrap');

// ------- *
// Plugins |
// ------- *

sync(store, router);

Vue.use(AsyncComputed);

Vue.use(BootstrapVue);

Vue.use(VueI18n);

Vue.use(ElementUI, { locale });

Vue.use(require('vue-moment'));

library.add(
  faTimes,
  faAngleUp,
  faAngleDown,
  faEye,
  faEyeSlash,
  faCheck,
  faInfo,
  faInfoCircle,
  faPowerOff
);

Raven.config(window.SENTRY_PUBLIC_DSN)
     .addPlugin(RavenVue, Vue)
     .install();

// --------- *
// Component |
// --------- *

Vue.component('font-awesome-icon', FontAwesomeIcon);

// ---------- *
// Directives |
// ---------- *

Vue.directive('outside-click', ClickOutside);

// ------------------------------- Launch app ------------------------------- //
// ========================================================================== //

// Translation plugin
const i18n = new VueI18n({
  locale: 'fr',
  messages,
});

/* eslint-disable no-new */
new Vue({ // Start the main application
  i18n,
  el: '#app',
  router,
  store,
  render: h => h(App) // Replace the content of <div id="app"></div> with App
});

// ----------------------------- Service worker ----------------------------- //
// ========================================================================== //

/** ---------------------------------------------------------------------------<
* /!\ In local, serviceWorker is supported only on http://localhost /!\
>--------------------------------------------------------------------------- **/

if('serviceWorker' in navigator) { // check browser compatability

  navigator.serviceWorker.register('/service-worker.js') // register your service worker file
                         .then((reg) => { // success
                           console.log('Service worker registered! Scope : ' + reg.scope);
                         })
                         .catch((err) => { // failure
                           console.log('Registration failed! ' + err);
                         });

}
else {

    console.log('The browser does not support Service Worker');

}
