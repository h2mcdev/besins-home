// --------------------------------- Imports -------------------------------- //
// ========================================================================== //

// Dependencies ------------------------------------------------------------- //

import Vue from 'vue';

import VueRouter from 'vue-router';

// Components --------------------------------------------------------------- //

// Home
import Home from './components/Home.vue';

// Login
import Login from './components/Login.vue';

// App ---------------------------------------------------------------------- //

import store from './store';

import {
  APP_NAME
} from './config';

// Const -------------------------------------------------------------------- //

import * as action_types from './store/actions-types';

// ------------------------------ Installation ------------------------------ //
// ========================================================================== //

// ---------- *
// Vue plugin |
// ---------- *

Vue.use(VueRouter);

// -------------------------------- Functions ------------------------------- //
// ========================================================================== //

/**
 * Check if user is not logged, redirect to login page
 *
 * @param  Route    to
 * @param  Route    from
 * @param  Function next
 * @return void
 */
function require_auth(to, from, next) {

  if(!store.getters.logged_in)
  {
    next({
      path: '/login',
      query: {
        redirect: to.fullPath,
        pathname: location.pathname // => For "work_as" feature
      }
    });
  }
  else {
    next();
  }

}

/**
 * Check if user is logged, redirect to home page
 *
 * @param  Route    to
 * @param  Route    from
 * @param  Function next
 * @return void
 */
function redirect_if_logged_in(to, from, next) {

  if(store.getters.logged_in)
  {
    next({ path: '/' });
  }
  else {
    next();
  }

}

/**
 * Logout user and redirect to login view
 *
 * @param  Route    to
 * @param  Route    from
 * @param  Function next
 * @return void
 */
function logout(to, from, next) {

  store.dispatch(action_types.LOGOUT_USER);

  next({ path: '/login', query: { auto: '0' } });

}

// ------------------------------ Launch router ----------------------------- //
// ========================================================================== //

export default new VueRouter({
  // Settings --------------------------------------------------------------- //
  // ------------------------------------------------------------------------ //
  mode: 'history',
  base: __dirname,
  // Routes ----------------------------------------------------------------- //
  // ------------------------------------------------------------------------ //
  routes: [
    // Default (Home) ------------------------------------------------------- //
    {
      name: 'home',
      path: '/',
      component: Home,
      beforeEnter: require_auth,
    },
    // Login ---------------------------------------------------------------- //
    {
      path: '/login',
      component: Login,
      beforeEnter: redirect_if_logged_in,
      props: (route) => ({
        redirect: route.query.redirect,
        pathname: route.query.pathname,
        auto: route.query.auto,
      })
    },
    {
      path: '/logout',
      beforeEnter: logout
    },
    // Not found ------------------------------------------------------------ //
    {
      path: '*',
      beforeEnter(to, from, next) {

        next('/');

      }
    },
  ]
});
