// --------------------------------- Imports -------------------------------- //
// ========================================================================== //

import {
  APP_STATUS_ONLINE,
  APP_STATUS_OFFLINE,
  APP_STATUS_LOADING,
  APP_STATUS_SYNCING,
  APP_STATUS_SYNC_DONE,
} from './status';

// ---------------------------------- Const --------------------------------- //
// -------------------------------------------------------------------------- //

/**
* Allow to disabled 'work_as' feature.
*
* @const Boolean
*/
const WORK_AS_ENABLED = false;

// --------------------------------- Getters -------------------------------- //
// ========================================================================== //

// ------------------------------ Online status ----------------------------- //
// -------------------------------------------------------------------------- //

/**
 * Check if app is online
 *
 * @param  Object { store.state } (implicit)
 * @return Boolean
 */
export const is_online = (state) => {

  return state.online_status === APP_STATUS_ONLINE;

};

/**
 * Check if app is offline
 *
 * @param  Object { store.state } (implicit)
 * @return Boolean
 */
export const is_offline = (state) => {

  return state.online_status === APP_STATUS_OFFLINE;

};

// --------------------------------- Login ---------------------------------- //
// -------------------------------------------------------------------------- //

/**
 * App on login attempt
 *
 * @param  Object { store.state } (implicit)
 * @return Boolean
 */
export const is_login_attempting = (state) => {

  return state.auto_reconnect === APP_STATUS_LOADING;

};

// ---------------------------------- User ---------------------------------- //
// -------------------------------------------------------------------------- //

/**
 * Check if user is logged
 *
 * @param  Object { store.state } (implicit)
 * @return Boolean
 */
export const logged_in = (state) => {

  return state.user !== false;

};

/**
 * Check if user is working as
 *
 * @param  Object { store.state } (implicit)
 * @return Boolean
 */
export const is_working_as = (state) => {

  return state.work_as > 0;

};

/**
 * Get user 'can_work_as' attributes
 *
 * @param  Object { store.state } (implicit)
 * @return Boolean
 */
export const can_work_as = (state) => {

  return WORK_AS_ENABLED && state.user.can_work_as;

};

// ---------------------------------- Sync ---------------------------------- //
// -------------------------------------------------------------------------- //

/**
 * Check if app is syncing
 *
 * @param  Object { store.state } (implicit)
 * @return Boolean
 */
export const is_syncing = (state) => {

  return state.sync_status === APP_STATUS_SYNCING;

};

/**
 * Check if app is synced
 *
 * @param  Object { store.state } (implicit)
 * @return Boolean
 */
export const is_synced = (state) => {

  return state.sync_status === APP_STATUS_SYNC_DONE;

};

// ------------------------------ Modules list ------------------------------ //
// -------------------------------------------------------------------------- //

/**
 * Get module list to clear
 *
 * @param  Object { store.state } (implicit)
 * @return Array
 */
export const modules_to_clear = (state) => {

  return state.filter((module_name) => {

    return [
      // 'module_name', (example)
      // ...
    ].includes(module_name);

  });

};

/**
 * Get module list to synchronize.
 * Could be filtered depending to (Example...)
 *
 * @param  Object { store.state } (implicit)
 * @return Array
 */
export const modules_to_synchronize = (state) => {

  return state.filter((module_name) => {

    // Example...
    // if(['admin'].includes(state.user.role)) {
    //
    //   return !['module_name (with_too_much_data)'].includes(module_name);
    //
    // }

    // Default
    return true;

  });

};
