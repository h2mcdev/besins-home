// Login -------------------------------------------------------------------- //

export const LOGIN_ATTEMPT = 'LOGIN_ATTEMPT';
export const LOGIN_USER    = 'LOGIN_USER';
export const LOGOUT_USER   = 'LOGOUT_USER';

// User --------------------------------------------------------------------- //

export const STORE_USER_CREDENTIALS = 'STORE_USER_CREDENTIALS';

// Sync --------------------------------------------------------------------- //

export const APP_SYNC         = 'APP_SYNC';
export const CHECK_IF_ONLINE  = 'CHECK_IF_ONLINE'; // (= determineIfOnline)
export const UPLOAD_MUTATIONS = 'UPLOAD_MUTATIONS';
export const CLEAR_SOME_DATA  = 'CLEAR_SOME_DATA';
export const SYNCHRONIZE_DATA = 'SYNCHRONIZE_DATA';

// Config ------------------------------------------------------------------- //

export const LOAD_CONFIGURATION = 'LOAD_CONFIGURATION';

// Messages ----------------------------------------------------------------- //

export const DISPLAY_MESSAGE = 'DISPLAY_MESSAGE';
