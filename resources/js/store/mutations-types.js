// App ---------------------------------------------------------------------- //

// On / Off
export const APP_IS_OFFLINE = 'APP_IS_OFFLINE';
export const APP_IS_ONLINE = 'APP_IS_ONLINE';

// Sync
export const APP_SYNCING   = 'APP_SYNCING';
export const APP_SYNC_DONE = 'APP_SYNC_DONE';

// User --------------------------------------------------------------------- //

export const USER_LOGIN_SUCCESS        = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILURE        = 'USER_LOGIN_FAILURE';
export const USER_LOGOUT               = 'USER_LOGOUT';
export const USER_UPDATE               = 'USER_UPDATE';
export const USER_RECONNECT_ATTEMPTING = 'USER_RECONNECT_ATTEMPTING';
export const USER_RECONNECT_ATTEMPTED  = 'USER_RECONNECT_ATTEMPTED';

// Config ------------------------------------------------------------------- //

export const SET_CONFIG = 'SET_CONFIG';

// Messages ----------------------------------------------------------------- //

export const ADD_MESSAGE    = 'ADD_MESSAGE';
export const REMOVE_MESSAGE = 'REMOVE_MESSAGE';
