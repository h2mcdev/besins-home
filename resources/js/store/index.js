// --------------------------------- Imports -------------------------------- //
// ========================================================================== //

// Dependencies ------------------------------------------------------------- //

import Vue from 'vue';
import Vuex from 'vuex';

// App ---------------------------------------------------------------------- //

import actions from './actions';

import mutations from './mutations';

import * as getters from './getters';

import {
  APP_STATUS_LOADED,
  APP_STATUS_ONLINE,
  APP_STATUS_SYNC_DONE
} from './status';

// ------------------------------ Installation ------------------------------ //
// ========================================================================== //

// ---------- *
// Vue plugin |
// ---------- *

Vue.use(Vuex); // Docs : https://vuex.vuejs.org/

// ------------------------------ Launch store ------------------------------ //
// ========================================================================== //

// Parameters --------------------------------------------------------------- //

// Default store state
const state = {
  user: false,
  token: false,
  auto_reconnect: APP_STATUS_LOADED,
  online_status: APP_STATUS_ONLINE,
  sync_status: APP_STATUS_SYNC_DONE,
  config: {
    //...
  },
  messages: [],
  module_list: [
    // 'module_name', (example)
    // ...
  ]
};

// Component ---------------------------------------------------------------- //

const store = new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
  strict: process.env.NODE_ENV !== 'production'
});

export default store;
