// --------------------------------- Imports -------------------------------- //
// ========================================================================== //

"use strict";

// Const -------------------------------------------------------------------- //

import * as types from './mutations-types';

import {
  APP_STATUS_ONLINE,
  APP_STATUS_OFFLINE,
  APP_STATUS_LOADED,
  APP_STATUS_LOADING,
  APP_STATUS_SYNCING,
  APP_STATUS_SYNC_DONE
} from './status';

// Initial data ------------------------------------------------------------- //

let last_offline_message_id = null;
let last_online_message_id  = null;

// --------------------------------- Object --------------------------------- //
// ========================================================================== //

/**
 * Mutations list:
 * -* App
 *  |- APP_IS_ONLINE
 *  |- APP_IS_OFFLINE
 * -* Sync
 *  |- APP_SYNCING
 *  |- APP_SYNC_DONE
 * -* User
 *  |- USER_LOGIN_SUCCESS
 *  |- USER_LOGOUT
 *  |- USER_RECONNECT_ATTEMPTING
 *  |- USER_RECONNECT_ATTEMPTED
 * -* Messages
 *  |- ADD_MESSAGE
 *  |- REMOVE_MESSAGE
 */
export default {

// ----------------------------------- App ---------------------------------- //
// -------------------------------------------------------------------------- //

  // On / Off --------------------------------------------------------------- //

  /**
   * Set store.state.online_status to 'ONLINE',
   * Set store.state.token
   * Set store.state.messages
   *
   * @param  Object { store.state } (implicit)
   * @param  Object { token: String }
   * @return void
   */
  [types.APP_IS_ONLINE]: (state, { token }) => {

    // Check app state is currentyl offline
    if(state.online_status === APP_STATUS_OFFLINE) {

      // Set store.state.messages, Remove last offline message
      state.messages = state.messages.filter((message) => message.id !== last_offline_message_id);

      // Set 'last_online_message_id'
      last_online_message_id = Date.now();

      // Add online message
      state.messages.push({
        type: 'success',
        message: 'Vous êtes connecté',
        id: last_online_message_id
      });

    }

    state.online_status = APP_STATUS_ONLINE;

    state.token = token;

  },

  /**
   * Set store.state.online_status to 'OFFLINE',
   * Set store.state.messages
   *
   * @param  Object { store.state } (implicit)
   * @return void
   */
  [types.APP_IS_OFFLINE]: (state) => {

    // Check app state is currentyl online
    if(state.online_status === APP_STATUS_ONLINE) {

      // Set store.state.messages, Remove last online message
      state.messages = state.messages.filter((message) => message.id !== last_online_message_id);

      // Set 'last_offline_message_id'
      last_offline_message_id = Date.now();

      // Add offline message
      state.messages.push({
        type: 'danger',
        message: 'Vous êtes hors ligne.',
        id: last_offline_message_id
      });

    }

    state.online_status = APP_STATUS_OFFLINE;

  },

  // Sync ------------------------------------------------------------------- //

  /**
   * Set store.state.sync_status to 'SYNCING'
   *
   * @param  Object { store.state } (implicit)
   * @return void
   */
  [types.APP_SYNCING]: (state) => {

    state.sync_status = APP_STATUS_SYNCING;

  },

  /**
   * Set store.state.sync_status to 'SYNC_DONE'
   *
   * @param  Object { store.state } (implicit)
   * @return void
   */
  [types.APP_SYNC_DONE]: (state) => {

    state.sync_status = APP_STATUS_SYNC_DONE;

  },

// ---------------------------------- User ---------------------------------- //
// -------------------------------------------------------------------------- //

  /**
   * Set store.state.user & store.state.token
   *
   * @param  Object { store.state } (implicit)
   * @param  Object { user: Object, token: String }
   * @return void
   */
  [types.USER_LOGIN_SUCCESS]: (state, { user, token }) => {

    state.user = user;

    state.token = token;

  },

  /**
   * Unset store.state.user & store.state.token
   *
   * @param  Object { store.state } (implicit)
   * @return void
   */
  [types.USER_LOGOUT]: (state) => {

    state.user = false;

    state.token = false;

  },

  /**
   * Set store.state.auto_reconnect to 'LOADING'
   *
   * @param  Object { store.state } (implicit)
   * @return void
   */
  [types.USER_RECONNECT_ATTEMPTING]: (state) => {

    state.auto_reconnect = APP_STATUS_LOADING;

  },

  /**
   * Set store.state.auto_reconnect to 'LOADED'
   *
   * @param  Object { store.state } (implicit)
   * @return void
   */
  [types.USER_RECONNECT_ATTEMPTED]: (state) => {

    state.auto_reconnect = APP_STATUS_LOADED;

  },

// -------------------------------- Message --------------------------------- //
// -------------------------------------------------------------------------- //

  /**
   * Set store.state.messages, Push message
   *
   * @param  Object { store.state } (implicit)
   * @param  Object message
   * @return void
   */
  [types.ADD_MESSAGE]: (state, message) => {

    state.messages.push(message);

  },

  /**
   * Set store.state.messages, Remove message by id
   *
   * @param  Object { store.state } (implicit)
   * @param  Number message_id
   * @return void
   */
  [types.REMOVE_MESSAGE]: (state, message_id) => {

    state.messages = state.messages.filter((message) => message.id !== message_id);

  },

}
