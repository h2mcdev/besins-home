// Online / Offline --------------------------------------------------------- //

export const APP_STATUS_ONLINE  = 'ONLINE';
export const APP_STATUS_OFFLINE = 'OFFLINE';

// Status ------------------------------------------------------------------- //

export const APP_STATUS_LOADED    = 'LOADED';
export const APP_STATUS_LOADING   = 'LOADING';
export const APP_STATUS_FAILURE   = 'FAILURE';
export const APP_STATUS_SYNCING   = 'SYNCING';
export const APP_STATUS_SYNC_DONE = 'SYNC_DONE';
