// --------------------------------- Imports -------------------------------- //
// ========================================================================== //

// App ---------------------------------------------------------------------- //

import api from '../api';

// Node modules ------------------------------------------------------------- //

import bcrypt from 'twin-bcrypt';

// Const -------------------------------------------------------------------- //

import * as mutations_types from './mutations-types';

import * as action_types from './actions-types';

import * as config from '../config';

// ---------------------------------- Const --------------------------------- //
// ========================================================================== //

/**
 * Get 6 hour in milliseconds
 */
const SIX_HOURS_IN_MILLISECONDS = 6 * 60 * 60 * 1000;

// --------------------------------- Object --------------------------------- //
// ========================================================================== //

/**
 * Actions list:
 * -* Login
 *  |- LOGIN_USER
 *  |- LOGOUT_USER
 * -* User
 *  |- STORE_USER_CREDENTIALS
 * -* Sync
 *  |- CHECK_IF_ONLINE
 *  |- APP_SYNC
 *  |- UPLOAD_MUTATIONS
 *  |- CLEAR_SOME_DATA
 *  |- SYNCHRONIZE_DATA
 * -* Config
 *  |- LOAD_CONFIGURATION
 * -* Messages
 *  |- DISPLAY_MESSAGE = managing message displaying
 */
export default {

// ---------------------------------- Login --------------------------------- //
// -------------------------------------------------------------------------- //

  /**
   * Log the user in, if it succeeds then :
   *
   * - store the token for future reconnections and offline authentification
   * - trigger a sync
   *
   * @param  Object { store.getters, store.commit, store.dispatch } (implicit)
   * @param  Object { email: String, password: String }
   * @return Object|void Promise
   */
  [action_types.LOGIN_USER]: ({ getters, commit, dispatch }, { email, password }) => {

    return new Promise((resolve, reject) => {

      // Parameters --------------------------------------------------------- //

      /**
       * Define failure function
       *
       * @return void
       */
      const failure = () => {

        // Set store.state.auto_reconnect to 'USER_RECONNECT_ATTEMPTED'
        commit(mutations_types.USER_RECONNECT_ATTEMPTED);

        // Display error message
        dispatch(action_types.DISPLAY_MESSAGE, {
          type: 'danger',
          message: 'Echec de l\'authentification.'
        });

      };

      /**
       * Define success function
       *
       * @param  Object { expires_in: Number, token: String, user: Object }
       * @return Object Promise
       */
      const success = (user_data) => {

        // Set store.state.user & store.state.token
        commit(mutations_types.USER_LOGIN_SUCCESS, user_data);

        // Set user_credentials & user_data in local storage
        dispatch(action_types.STORE_USER_CREDENTIALS, { email, password, user_data });

        // Set store.state.config
        dispatch(action_types.LOAD_CONFIGURATION).then(() => {

          // Sync modules datas
          return dispatch(action_types.APP_SYNC);

        }).then(() => {

          // Set store.state.auto_reconnect to 'USER_RECONNECT_ATTEMPTED'
          commit(mutations_types.USER_RECONNECT_ATTEMPTED);

          // Resolve Promise
          resolve();

        });

      };

      // Process ------------------------------------------------------------ //

      // Set store.state.auto_reconnect to 'USER_RECONNECT_ATTEMPTING'
      commit(mutations_types.USER_RECONNECT_ATTEMPTING);

      // Check app is online & user is logged
      dispatch(action_types.CHECK_IF_ONLINE).then((is_online) => {

        if(!is_online) // Offline : Login from local storage
        {
          try
          {
            bcrypt.compare(
              email + password,
              localStorage.getItem(config.APP_PREFIX + 'user_credentials'),
              () => {},
              (both_match) => {

                if(both_match)
                {
                  return success(JSON.parse(localStorage.getItem(config.APP_PREFIX + 'user_data')));
                }
                else {
                  return failure();
                }

              });

            }
            catch(e) { // Login failed

              failure();

            }
        }
        else { // Online : Login from Laravel

          fetch(config.API_URL + config.API_PATH + '/auth/login', {
            method: 'POST',
            body: JSON.stringify({ email, password }),
            headers: { 'Content-Type': 'application/json' }
          }).then((response) => {

            if(response.status != 200)
            {
              return failure();
            }
            else {

              response.json().then((json) => {

                // Set api.token
                api.set_token(json.token);
                api.set_autologin_token(json.user.app_autologin_token);

                // Set api.user_email
                api.set_user_email(json.user.email);

                // Set token
                localStorage.setItem(config.APP_PREFIX + 'token', json.token);

                return success(json);

              });

            }

          }).catch(function (err) {

            failure();

          });

        }

      });

    });

  },

  /**
   * Set user and token state to false
   * (optional) Reset user "work_as" data in remote database
   *
   * @param  Object { store.getters, store.commit, store.dispatch } (implicit)
   * @param  Object { email: String, password: String }
   * @return void Promise
   */
  [action_types.LOGOUT_USER]: ({ commit, dispatch, state }) => {

    // Reset user's work_as
    if(state.user.work_as) {

      api.set_work_as({ work_as: 0 });

    }

    // Set store.state.user & store.state.token to false
    commit(mutations_types.USER_LOGOUT);

    // Display message
    dispatch(action_types.DISPLAY_MESSAGE, {
      type: 'danger',
      message: "Vous avez été déconnecté."
    });

  },

  /**
   * Attempts to reconnect a user with the token in local storage
   *
   * @param  Object  { store.commit, store.dispatch, store.getters } (implicit)
   * @param  Boolean reset_work_as
   * @return void
   */
  [action_types.LOGIN_ATTEMPT]: ({ commit, dispatch, getters }, reset_work_as) => {

    // Stop if offline
    if(getters.is_offline) { return; }

    // Parameters ----------------------------------------------------------- //

    /**
     * Define failure function
     *
     * @param  String message
     * @return void
     */
    const failure = (message = '') => {

      // Set store.state.auto_reconnect to 'USER_RECONNECT_ATTEMPTED'
      commit(mutations_types.USER_RECONNECT_ATTEMPTED);

      if(message) {

        // Display error message
        dispatch(action_types.DISPLAY_MESSAGE, {
          type: 'danger',
          message: message
        });

      }

    };

    // Get token
    const token_from_local_storage = localStorage.getItem(config.APP_PREFIX + 'token');

    // Process -------------------------------------------------------------- //

    // Set store.state.auto_reconnect to 'USER_RECONNECT_ATTEMPTING'
    commit(mutations_types.USER_RECONNECT_ATTEMPTING);

    // Check Token
    if(token_from_local_storage === null) {

      failure();

      return;

    }

    // Fetch auth user data
    fetch(config.API_URL + config.API_PATH + '/auth/refresh', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token_from_local_storage,
        'ResetWorkAs': reset_work_as // => For Middleware
      }
    }).then((response) => { // Success -------------------------------------- //

      // Token expired
      if(!response || response.status != 200)
      {

        failure();

      }
      else { // Valid token

        response.json().then((json) => {

          // Set api.token
          api.set_token(json.token);

          // Set api.user_email
          api.set_user_email(json.user.email);

          // Set store.state.user & store.state.token
          commit(mutations_types.USER_LOGIN_SUCCESS, json);

          // Set store.state.config
          dispatch(action_types.LOAD_CONFIGURATION).then(() => {

            // Sync modules datas
            return dispatch(action_types.APP_SYNC);

          }).then(() => {

            // Set store.state.auto_reconnect to 'USER_RECONNECT_ATTEMPTED'
            commit(mutations_types.USER_RECONNECT_ATTEMPTED)

          });

        });

      }

    }).catch(function (err) { // Failed ------------------------------------- //

      console.log(err); // => Debug

      failure('Echec d\'authentification automatique');

    });

  },

// ---------------------------------- User ---------------------------------- //
// -------------------------------------------------------------------------- //

  /**
   * Store user credential and data into local storage
   *
   * @param  Object { email: String, password: String, user_data: Object }
   * @return Object Promise()
   */
  [action_types.STORE_USER_CREDENTIALS]: ({}, { email, password, user_data }) => {

    return new Promise((resolve, reject) => {

      // Hash user's email + password
      bcrypt.hash(email + password, (hash) => {

        // Set user_credentials
        localStorage.setItem(config.APP_PREFIX + 'user_credentials', hash);

        // Set user_data
        localStorage.setItem(config.APP_PREFIX + 'user_data', JSON.stringify(user_data));

        resolve(hash);

      });

    });

  },

// ---------------------------------- Sync ---------------------------------- //
// -------------------------------------------------------------------------- //

  /**
   * Check if the app is online and the user logged in
   * Refresh the token on success
   *
   * @param  Object { store.state, store.commit } (implicit)
   * @return Promise(Boolean)
   */
  [action_types.CHECK_IF_ONLINE]: ({ state, commit }) => {

    return new Promise((resolve, reject) => {

      // Navigator is offline ----------------------------------------------- //

      if(!navigator.onLine) {

        // Set state.store.online_status to 'OFFLINE'
        commit(mutations_types.APP_IS_OFFLINE);

        // Resolve promise with FALSE response
        resolve(false);

        // Stop promise
        return;

      }

      // Navigator is online ------------------------------------------------ //

      // Check connection by Laravel Auth
      fetch(config.API_URL + config.API_PATH + '/auth/online', {
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + state.token
        }
      }).then((response) => { // Success ------------------------------------ //

        const new_token = (response.status === 200) ? response.headers.get('Authorization').slice(7) : '';

        // Set store.state.online_status & store.state.token
        commit(mutations_types.APP_IS_ONLINE, { token: new_token });

        // Set token in local storage
        localStorage.setItem(config.APP_PREFIX + 'token', new_token);

        // Set api.token
        api.set_token(new_token);

        // For later...
        // api.setUseLocalDb(false);

        // Resolve promise with TRUE response
        resolve(true);

      }).catch((err) => { // Failed ----------------------------------------- //

        // Set store.state.online_status to 'OFFLINE'
        commit(mutations_types.APP_IS_OFFLINE);

        // For later...
        // api.setUseLocalDb(true);

        // Resolve promise with FALSE response
        resolve(false);

      });

    });

  },

  /**
   * Main sync logic, calls all required actions in the right order
   *
   * @param  Object { store.state, store.commit, store.dispatch, store.getters } (implicit)
   * @return Object Promise()
   */
  [action_types.APP_SYNC]: ({ state, commit, dispatch, getters }) => {

    // App user's state is not setted --------------------------------------- //

    if(!getters.logged_in) {

      return new Promise((resolve, reject) => { resolve(); });

    }

    // App user's state is setted ------------------------------------------- //

    // Check app is not already on synchronizing
    if(!getters.is_syncing) {

      // Sync started _______________________________________________________ //

      // Set store.state.sync_status
      commit(mutations_types.APP_SYNCING);

      // Check app is online & user is logged
      const promise = dispatch(action_types.CHECK_IF_ONLINE).then((is_online) => {

        if(is_online) {

          // Upload mutations
          return dispatch(action_types.UPLOAD_MUTATIONS).then(() => {

            // Clear some data (user is working as)
            return dispatch(action_types.CLEAR_SOME_DATA);

          }).then(() => {

            // Synchronize data
            return dispatch(action_types.SYNCHRONIZE_DATA);

          });

        }

      });

      // Sync done __________________________________________________________ //

      return promise.then(() => { // Success -------------------------------- //

        // Set store.state.sync_status to 'SYNC_DONE'
        commit(mutations_types.APP_SYNC_DONE);

      }).catch((err) => { // Failed ----------------------------------------- //

        // Debug
        // console.log('Error while syncing: ' + err);

        // Set store.state.sync_status to 'SYNC_DONE'
        commit(mutations_types.APP_SYNC_DONE);

        // Display error message
        dispatch(action_types.DISPLAY_MESSAGE, {
          type: 'danger',
          message: 'Echec de la synchronisation, certaines données n\'ont pas pu être enregistrées ou téléchargées.'
        });

      });

    }

  },

 /**
  * Upload mutations stored in Dexie database
  *
  * @param  Object { store.state } (implicit)
  * @return Promise.all()
  */
  [action_types.UPLOAD_MUTATIONS]: ({ state }) => {

    return new Promise((resolve, reject) => { resolve(); });

    // For later... --------------------------------------------------------- //

    // // Check "mutationsCounter" > 0
    // if(api.mutationsUploading())
    // {
    //   return new Promise((resolve) => setTimeout(resolve, 1000));
    // }
    // else { // "mutationsCounter" = 0
    //
    //   // Fetch mutations in local database
    //   return db.mutations.toArray().then((mutations) => {
    //
    //     // Upload each mutation
    //     const promisesToWaitFor = mutations.map((mutation) => {
    //
    //       return api.uploadMutation(mutation);
    //
    //     });
    //
    //     return Promise.all(promisesToWaitFor);
    //
    //   });
    //
    // }

  },

  /**
   * Clear too large data in local Dexie database
   *
   * @param  Object { store.getters } (implicit)
   * @return Object Promise()
   */
  [action_types.CLEAR_SOME_DATA]: ({ getters }) => {

    // For later... --------------------------------------------------------- //

    // // Check if user is working as
    // if(getters.is_working_as) {
    //
    //   // Parameters --------------------------------------------------------- //
    //
    //   // Init array promises
    //   const promises_to_wait_for = [];
    //
    //   // Clear module table in Dexie DB
    //   getters.modules_to_clear.forEach((module_name) => {
    //
    //     promises_to_wait_for.push(api.clearModule(module_name));
    //
    //   });
    //
    //   // Return ------------------------------------------------------------- //
    //   // -------------------------------------------------------------------- //
    //
    //   return Promise.all(promises_to_wait_for).then(() => {
    //
    //     return Promise.resolve();
    //
    //   });
    //
    // }

    // Default
    return Promise.resolve();

  },

  /**
   * Synchronize module data, fetch from remote and save in local Dexie database.
   *
   * @param  Object { store.state, store.getters, store.dispatch } (implicit)
   * @return Object Promise.all()
   */
  [action_types.SYNCHRONIZE_DATA]: ({ state, getters, dispatch }) => {

    return Promise.resolve();

    // For later... --------------------------------------------------------- //

    // // Parameters ----------------------------------------------------------- //
    //
    // const promises_to_wait_for = [];
    //
    // // Synchronize ---------------------------------------------------------- //
    //
    // getters.modules_to_synchronize.forEach((module_name) => {
    //
    //   // Local storage module informations ---------------------------------- //
    //
    //   // (example) = "user@mail.com#2208988800"
    //   const module_last_updating = localStorage.getItem(config.APP_PREFIX + 'last_updated_' + module_name);
    //
    //   // (example) = "user@mail.com"
    //   let email_last_updated = (module_last_updating || '#').split('#')[0];
    //
    //   // (example) = "2208988800"
    //   let timestamp_last_updated = (module_last_updating || '#').split('#')[1];
    //
    //   // Conditions for Synchronization ------------------------------------- //
    //
    //   // 1 - User has changed
    //   const user_changed = email_last_updated != state.user.email;
    //
    //   // 2 - Waiting time (More than 6 hours)
    //   const waiting_time = Date.now() - timestamp_last_updated > SIX_HOURS_IN_MILLISECONDS;
    //
    //   // Update module ------------------------------------------------------ //
    //
    //   if(user_changed || waiting_time) {
    //
    //     // Init ajax params
    //     const params = {
    //       sync: true,
    //       filter: {}
    //     };
    //
    //     // Example module -------------------------------------------- //
    //
    //     // if(module_name === 'example') {
    //     //
    //     //   params.filter['field'] = {
    //     //     type: 'field',
    //     //     value: state.user.email
    //     //   };
    //     //
    //     // }
    //
    //     // Fetch data from database
    //     // promises_to_wait_for.push(api.fetch(module_name, params));
    //
    //   }
    //
    // });
    //
    // return Promise.all(promises_to_wait_for).then(() => {
    //
    //   // Update module data in local Dexie database
    //   // To do... (Return promise from Dexie functions)
    //
    // });

  },

// --------------------------------- Config --------------------------------- //
// -------------------------------------------------------------------------- //

  /**
   * Set store.state.config
   *
   * Config list:
   * -* example (No config for now)
   *
   * @param  Object { store.commit, store.dispatch, store.state } (implicit)
   * @return Object Promise.all()
   */
  [action_types.LOAD_CONFIGURATION]: ({ commit, dispatch, state }) => {

    // Init array promise
    const promises_to_wait_for = [];

    // Set state ------------------------------------------------------------ //

    // Set store.state.config.example
    // promises_to_wait_for.push(api.get('example', {}).then(({ data }) => {
    //
    //   commit(mutations_types.SET_CONFIG, { example: data });
    //
    // }));

    return Promise.all(promises_to_wait_for);

  },

// --------------------------------- Message -------------------------------- //
// -------------------------------------------------------------------------- //

  /**
   * Manage message to add in the store's state, display it, remove it...
   *
   * @param  Object { store.commit } (implicit)
   * @param  Object  message = message to add in store state
   * @return Object Promise()
   */
  [action_types.DISPLAY_MESSAGE]: ({ commit }, message) => {

    // Parameters ----------------------------------------------------------- //

    const message_id = Date.now();

    const countdown = _.has(message, 'countdown') ? message.countdown : 5000;

    // Process -------------------------------------------------------------- //

    // Set store.state.messages, add message
    commit(mutations_types.ADD_MESSAGE, { id: message_id, ...message });

    // Set store.state.messages, remove message after the countdown
    setTimeout(() => commit(mutations_types.REMOVE_MESSAGE, message_id), countdown);

    // ---------------------------------------------------------------------- //

    return Promise.resolve();

  },

};
