// Url ---------------------------------------------------------------------- //

// Remote (Prod)
export const API_URL       = window.location.origin;
export const CRM_URL       = 'https://crm-besins-test-scwy.repsview.com/';
export const ORDERS_URL    = 'https://orders-besins-test.repsview.com/';
export const CR35_URL      = 'https://cr35-besins-test-scw.repsview.com/';
export const DASHBOARD_URL = 'https://repsview-besins-test.repsview.com/';

// Remote (Test)
// export const API_URL       = 'https://home-grimberg-test.repsview.com/';
// export const CRM_URL       = 'https://crm-grimberg-test.repsview.com/';
// export const ORDERS_URL    = 'https://neworders-grimberg-test.repsview.com/';
// export const CR35_URL      = 'https://cr35-grimberg-test.repsview.com/';
// export const DASHBOARD_URL = 'https://dashboard-grimberg-test.repsview.com/';
// export const CIBLAGE_URL   = 'https://ciblage-grimberg-test.repsview.com/';

// Local (Mac)
// export const API_URL       = 'http://dev.grimberg.home:8000';
// export const CRM_URL       = 'http://dev.grimberg.crm:8001';
// export const ORDERS_URL    = 'http://dev.grimberg.orders:8002';
// export const CR35_URL      = 'http://dev.grimberg.cr35:8003';
// export const DASHBOARD_URL = 'http://dev.grimberg.dashboard:8004';
// export const CIBLAGE_URL   = 'http://dev.grimberg.ciblage:8005';


// App ---------------------------------------------------------------------- //

export const APP_NAME   = 'Besins';
export const APP_PREFIX = 'besins_home_';

// Path --------------------------------------------------------------------- //

export const API_PATH  = '/api';
