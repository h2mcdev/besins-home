import { precacheAndRoute, cleanupOutdatedCaches } from 'workbox-precaching';
import {skipWaiting, clientsClaim} from 'workbox-core';
skipWaiting();
clientsClaim();
cleanupOutdatedCaches();

// --------------------------- PRECACHING ---------------------------- //

// To change the default //css/app.css and add the prefix url
const processedManifest = (self.__WB_MANIFEST || []).map(entry => {
  if(entry.url == "//css/app.css")
    entry.url = "/css/app.css" 
  if(entry.url == "//js/app.js")
    entry.url = "/js/app.js" 
  if(entry.url == "//js/app.js.LICENSE.txt")
    entry.url = "/js/app.js.LICENSE.txt" 
  
  entry.url = entry.url;
  return entry;
});
precacheAndRoute(processedManifest);



// Precaching app, homepage and images
precacheAndRoute([
  {url : "/", revision: Math.random().toString(36).substring(2)},
  {url : "/home", revision: Math.random().toString(36).substring(2)},
  //{url : "/js/app.js"},
  {url : "/manifest.webmanifest", revision: Math.random().toString(36).substring(2)},
   
 
  // Images
  {url: "/images/favicon/ms-icon-144x144.png", revision: Math.random().toString(36).substring(2)},
  {url: "/images/logo_dashboard.png", revision: Math.random().toString(36).substring(2)},
  {url: "/images/logo_ciblage.png", revision: Math.random().toString(36).substring(2)},
  {url: "/images/logo_cr35.png", revision: Math.random().toString(36).substring(2)},
  {url: "/images/logo_crm.png", revision: Math.random().toString(36).substring(2)},
  {url: "/images/logo_orders.png", revision: Math.random().toString(36).substring(2)},
  {url: "/images/logo.png", revision: Math.random().toString(36).substring(2)},
  ]
)


// ------------- RUN TIME CATCHING WE DONT NEED FOR NOW ---------------- //


// import { setCatchHandler } from 'workbox-routing';
import { registerRoute } from 'workbox-routing';
import { NetworkFirst } from 'workbox-strategies';

// // Used for filtering matches based on status code, header, or both
import { CacheableResponsePlugin } from 'workbox-cacheable-response';
import { BroadcastCacheUpdate } from 'workbox-broadcast-update';
// // Used to limit entries in cache, remove entries after a certain period of time
// import { ExpirationPlugin } from 'workbox-expiration';

// Cache Requests with a Network First strategy
registerRoute(
  // Check to see if the request is a navigation to a new page
  ({url}) => url.pathname.startsWith('/api/get_'),
  // Use a Network First caching strategy
  new NetworkFirst({
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new CacheableResponsePlugin({
        statuses: [200],
      }),
      new BroadcastCacheUpdate()
    ],
  })
);


// // Cache CSS, JS, and Web Worker requests with a Stale While Revalidate strategy
// registerRoute(
//   // Check to see if the request's destination is style for stylesheets, script for JavaScript, or worker for web worker
//   ({ request }) => request.destination === 'worker',
//   // Use a Stale While Revalidate caching strategy
//   new StaleWhileRevalidate({
//     plugins: [
//       // Ensure that only requests that result in a 200 status are cached
//       new CacheableResponsePlugin({
//         statuses: [200],
//       })
//     ],
//   }),
// );

// // Cache images with a Cache First strategy
// registerRoute(
//   // Check to see if the request's destination is style for an image
//   ({ request }) => request.destination === 'image',
//   // Use a Cache First caching strategy
//   new CacheFirst({
//     // Put all cached files in a cache named 'images'
//     cacheName: 'images',
//     plugins: [
//       // Ensure that only requests that result in a 200 status are cached
//       new CacheableResponsePlugin({
//         statuses: [200],
//       }),
//       // Don't cache more than 50 items, and expire them after 30 days
//       new ExpirationPlugin({
//         maxEntries: 50,
//         maxAgeSeconds: 60 * 60 * 24 * 30, // 30 Days
//       }),
//     ],
//   }),
// );

// // Cache images with a Cache First strategy
// registerRoute(
//   // Check to see if the request's destination is style for an image
//  new RegExp('\\.js$'),
//   // Use a Cache First caching strategy
//   new CacheFirst({
//     // Put all cached files in a cache named 'images'
//     cacheName: 'images',
//     plugins: [
//       // Ensure that only requests that result in a 200 status are cached
//       new CacheableResponsePlugin({
//         statuses: [200],
//       }),
//       // Don't cache more than 50 items, and expire them after 30 days
//       new ExpirationPlugin({
//         maxEntries: 50,
//         maxAgeSeconds: 60 * 60 * 24 * 30, // 30 Days
//       }),
//     ],
//   }),
// );

